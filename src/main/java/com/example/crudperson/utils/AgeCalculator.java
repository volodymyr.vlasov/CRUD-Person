package com.example.crudperson.utils;

import java.time.LocalDateTime;

public class AgeCalculator {
    public static Integer calculateAgeByBirthDay(LocalDateTime dayOfBirth) {
        if (dayOfBirth != null && dayOfBirth.compareTo(LocalDateTime.now()) < 0) {
            return LocalDateTime.now().getYear() - dayOfBirth.getYear();
        }
        return null;
    }
}
