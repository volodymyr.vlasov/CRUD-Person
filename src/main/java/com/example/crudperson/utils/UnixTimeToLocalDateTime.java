package com.example.crudperson.utils;

import com.fasterxml.jackson.databind.util.StdConverter;

import java.time.Instant;
import java.time.LocalDateTime;
import java.util.TimeZone;

public class UnixTimeToLocalDateTime extends StdConverter<Long, LocalDateTime> {
    public LocalDateTime convert(final Long value) {
        return LocalDateTime.ofInstant(Instant.ofEpochSecond(value),
                TimeZone.getDefault().toZoneId());
    }
}
