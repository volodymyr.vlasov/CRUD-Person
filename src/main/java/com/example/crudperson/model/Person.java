package com.example.crudperson.model;

import com.example.crudperson.utils.UnixTimeToLocalDateTime;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "person")
@NoArgsConstructor
public class Person {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "age")
    private Integer age;
    @Column(name = "day_of_birth")
    @JsonDeserialize(converter = UnixTimeToLocalDateTime.class)
    private LocalDateTime dayOfBirth;
    @Column(name = "name")
    private String name;
    @Column(name = "last_name")
    private String lastName;
}
