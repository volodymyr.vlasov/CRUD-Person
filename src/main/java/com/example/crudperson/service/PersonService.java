package com.example.crudperson.service;

import com.example.crudperson.model.Person;

import java.util.List;

public interface PersonService {
    Person create(Person person);
    Person update(Person person);
    boolean delete(Long id);
    List<Person> getPersons();
    Person getPersonById(Long id);
}
