package com.example.crudperson.service.impl;

import com.example.crudperson.model.Person;
import com.example.crudperson.repository.PersonRepository;
import com.example.crudperson.service.PersonService;
import com.example.crudperson.utils.AgeCalculator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MySqlPersonService implements PersonService {

    @Autowired
    PersonRepository personRepository;

    public MySqlPersonService(PersonRepository personRepository) {
        this.personRepository = personRepository;
    }

    @Override
    public Person create(Person person) {
        if (person.getId() == null) {
            person.setAge(AgeCalculator.calculateAgeByBirthDay(person.getDayOfBirth()));
            return personRepository.save(person);
        }
        return null;
    }

    @Override
    public Person update(Person person) {
        if (personRepository.existsById(person.getId())) {
            return personRepository.save(person);
        }
        return null;
    }

    @Override
    public boolean delete(Long id) {
        personRepository.deleteById(id);
        return personRepository.existsById(id);
    }

    @Override
    public List<Person> getPersons() {
        return personRepository.findAll();
    }

    @Override
    public Person getPersonById(Long id) {
        if (personRepository.existsById(id)) {
            return personRepository.getById(id);
        }
        return null;
    }
}
